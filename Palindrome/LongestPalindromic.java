public  class LongestPalindromic {

    static String BruteForce(String word)
    {
        if (word == null)
            return null;

        if (word.isEmpty())
            return "";

        // each character in the word is a palindrome itself, so the default value of maxLength is 1
        // if no palindrome (with length > 1) is found, the first character will be returned, thus start = 0
        int maxLength = 1, start = 0;
        int n = word.length();

        // Nested loop to find the start and end index of the palindrome
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n; j++) {
                boolean palindrome = true;

                // Check the palindrome
                for (int k = 0; k < (j - i + 1) / 2; k++)
                    if (word.charAt(i + k) != word.charAt(j - k))
                        palindrome = false;

                // Save the palindrome until find à bigger one
                if (palindrome && (j - i + 1) > maxLength) {
                    start = i;
                    maxLength = j - i + 1;
                }
            }
        }
        // return the biggest palindrome
        return word.substring(start, start + maxLength);
    }


    static String FixTheCenter(String word) {

        if (word == null)   return  null;

        if (word.isEmpty()) return "";

        int n = word.length();

        // each character in the word is a palindrome itself, so the default value of maxLength is 1
        // if no palindrome (with length > 1) is found, the first character will be returned, thus start = 0
        int maxLength = 1,start=0;
        int low, high, i;

        for (i = 0; i < n; i++) {

            // Initialize low and high
            low = i - 1;
            high = i + 1;

            // Increment high until the value of the index high is the same as the value of the current index i
            while ( high < n && word.charAt(high) == word.charAt(i))
                high++;

            // Decrement low until the value of the index low is the same as the value of the current index i
            while ( low >= 0 && word.charAt(low) == word.charAt(i))
                low--;

            // Now as long as the value in index high is not the same as the value in index low, increment high and decrement low
            while (low >= 0 && high < n && word.charAt(low) == word.charAt(high) ){
                low--;
                high++;
            }

            // If the new palindrome is longer than the previous one, take it
            int length = high - low - 1;
            if (maxLength < length){
                maxLength = length;
                start=low+1;
            }
        }
        return word.substring(start, start + maxLength );
    }
}
