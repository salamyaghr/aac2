import java.util.*;


public class Convexe {

   

   int isOriented(int [] p1, int [] p2, int [] p3) {
      int val = (p2[1] - p1[1]) * (p3[0] - p2[0]) -
               (p2[0] - p1[0]) * (p3[1] - p2[1]);

      if (val < 0) return 2;
      else if (val > 0) return 1;
      else return 0;
      
   }

   
   ArrayList<int[]>  getConvexHull(int [][] points, int n) {

      if (n < 3) return null;

      ArrayList<int[]>  hull  = new ArrayList<int []>();
      int l = 0;
      for (int i = 1; i < n; i++) {
         if (points[i][0] < points[l][0]) l = i;
      }
         
      int p = l, q;
                       
      do {

         hull.add(points[p]);
         q = (p+1)%n;
         for (int i = 0; i < n; i++) {
            if (isOriented(points[p], points[i], points[q]) == 2) q = i;
         }
         p = q;
      } while (p != l);  
      
      return hull;
   }
   
   public static void main(String args[]) {

      Convexe convexe = new Convexe();
      int [][]  points = {{0,3},{2,2},{1,1}, {2,1}, {3,0}, {0,0},{3,3}};
      int [][]  points2 = {{-5, -3},{-3, -1},{-2, -2}, {-2, -1}, {-1, -5}, {-1, -3},{1, -1}, {-1, 1}, {0,0}, {1, -4}};
      
      int n = points.length/2;
      ArrayList<int[]> ans = convexe.getConvexHull(points2, points2.length);
     
      for (int[] e : ans) {
         System.out.println("[ " + e[0] + " ] , [ " + e[1]+ " ] , ");
     }
      
   }


     
}
