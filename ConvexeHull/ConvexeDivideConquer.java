import java.util.*;

public class ConvexeDivideConquer {
    
    private int [] middle = new int[2];

    int quad(int [] p) {
        if (p[0] >= 0 && p[1] >= 0) return 1;
        if (p[0] <= 0 && p[1] >= 0) return 2;
        if (p[0] <= 0 && p[1] <= 0) return 3;
        return 4;
    }

    int orientation(int [] a, int [] b, int []c) {
        int res = (b[1]-a[1])*(c[0]-b[0]) - (c[1]-b[1])*(b[0]-a[0]);

        if (res == 0) return 0;
        if (res > 0) return 1;
        return -1;
    }

    Boolean compare(int [] p1, int [] q1) {
        int [] p = new int[2];
        int [] q = new int[2];
        p[0] = p1[0] - middle[0];
        p[1] = p1[1] - middle[1];
        q[0] = q1[0] - middle[0];
        q[1] = q1[1] - middle[1];

        int one = quad(p);
        int two = quad(q);

        if (one != two) return (one < two);
        return (p[1]*q[0] < q[1]*p[0]);
    }

    ArrayList<int[]> merge(ArrayList<int[]> a, ArrayList<int[]> b) {
        
        int n1 = a.size(), n2 = b.size();

        int ia = 0, ib = 0;
        for (int i=1; i<n1; i++) {
            if (a.get(i)[0] > a.get(ia)[0]) ia = i;
        }
            

        // ib -> leftmost point of b
        for (int i=1; i<n2; i++) {
            if (b.get(i)[0] < b.get(ib)[0]) ib=i;
        }
            

        // finding the upper tangent
        int inda = ia, indb = ib;
        Boolean done = false;
        while (!done) {
            done = true;
            while (orientation(b.get(indb), a.get(inda), a.get((inda+1)%n1)) >=0)
                inda = (inda + 1) % n1;

            while (orientation(a.get(inda), b.get(indb), b.get((n2+indb-1)%n2)) <=0)
            {
                indb = (n2+indb-1)%n2;
                done = false;
            }
        }

        int uppera = inda, upperb = indb;
        inda = ia;
        indb=ib;
        done = false;
        int g = 0;
        
        while (!done) {
            done = true;
            while (orientation(a.get(inda), b.get(indb), b.get((indb+1)%n2))>=0) {

                indb=(indb+1)%n2;
            }

            while (orientation(b.get(indb), a.get(inda), a.get((n1+inda-1)%n1))<=0) {
                inda=(n1+inda-1)%n1;
                done=false;
            }
        }

        int lowera = inda, lowerb = indb;
        ArrayList<int[]> ret = new ArrayList<int[]>();

        int ind = uppera;
        ret.add(a.get(uppera));
        while (ind != lowera) {
            ind = (ind+1)%n1;
            ret.add(a.get(ind));
        }

        ind = lowerb;
        ret.add(b.get(lowerb));
        while (ind != upperb) {
            ind = (ind+1)%n2;
            ret.add(b.get(ind));
        }
        return ret;
    }
    

    void insertionSort(ArrayList<int[]> arr, int n)  { 
        if (n <= 1)  return;

        insertionSort( arr, n-1 );              
        int [] last = arr.get(n-1);                        
        int j = n-2;                                
       
        while (j >= 0 &&  this.compare(last,arr.get(j)))                 
        { 
            arr.set(j+1, arr.get(j));                  
            j--; 
        } 
        arr.set(j+1, last);                  
    } 

    ArrayList<int[]> bruteHull(ArrayList<int[]> a) {
        
        ArrayList<int[]> s = new ArrayList<int[]>();

        for (int i=0; i<a.size(); i++) {
            for (int j=i+1; j<a.size(); j++) {
                int x1 = a.get(i)[0], x2 = a.get(j)[0];
                int y1 = a.get(i)[1], y2 =a.get(j)[1];

                int a1 = y1-y2;
                int b1 = x2-x1;
                int c1 = x1*y2-y1*x2;
                int pos = 0, neg = 0;
                for (int k=0; k<a.size(); k++) {
                    if (a1*a.get(k)[0]+b1*a.get(k)[1]+c1 <= 0) neg++;
                    if (a1*a.get(k)[0]+b1*a.get(k)[1]+c1 >= 0) pos++;
                }
                if (pos == a.size() || neg == a.size())
                {
                    s.add(a.get(i));
                    s.add(a.get(j));
                }
            }
        }

        ArrayList<int[]> ret = new ArrayList<int[]>();
        
        for (int [] e : s) {
            ret.add(e);
        }
        

        // Sorting the points in the anti-clockwise order
        middle[0] = 0;
        middle[1] = 0;
        int n = ret.size();
        for (int i=0; i<n; i++) {
            middle[0] += ret.get(i)[0];
            middle[1] += ret.get(i)[1];
            ret.set(i,this.getPoint(ret.get(i)[0] * n, ret.get(i)[1] * n));
        }
        this.insertionSort(ret, n);
        for (int i=0; i<n; i++) {
            ret.set(i, this.getPoint(ret.get(i)[0]/n,  ret.get(i)[1]/n));
        }
        

        return ret;
    }


    ArrayList<int []> divide(ArrayList<int[]> a) {
        
        if (a.size() <= 5) return bruteHull(a);

        ArrayList<int[]> left = new ArrayList<int[]>();
        ArrayList<int[]> right = new ArrayList<int[]>();
        for (int i=0; i<a.size()/2; i++) {
            left.add(a.get(i));
        }
            
        for (int i=a.size()/2; i<a.size(); i++) {
            right.add(a.get(i));
        }
        

        
        ArrayList<int[]> left_hull = divide(left);
        ArrayList<int[]>  right_hull = divide(right);

        // merging the convex hulls
        return merge(left_hull, right_hull);
    }

int [] getPoint(int x, int y) {
    int [] pt = new int[2];
    pt[0] = x;
    pt[1] = y;
    return pt;
}

public static void main(String args[]) {
    ArrayList<int[]> a = new ArrayList<int[]>() ;
    ConvexeDivideConquer c = new ConvexeDivideConquer();
    

    a.add(c.getPoint(-5, -3));
    a.add(c.getPoint(-3, -1));
    a.add(c.getPoint(-2, -2));
    a.add(c.getPoint(-2, -1));
    a.add(c.getPoint(-1, -5));
    a.add(c.getPoint(-1, -3));
	a.add(c.getPoint(-1, -1));
	a.add(c.getPoint(-1, 1));
    a.add(c.getPoint(0, 0));
    a.add(c.getPoint(1, -4));
    
    

	int n = a.size();

	// sorting the set of points according
    // to the x-coordinate
    
    ArrayList<int[]> ans = c.divide(a);

	for (int[] e : ans) {
        System.out.println("[ " + e[0] + " ] , [ " + e[1]+ " ] , ");
    }
    
}




    

}


